const express = require('express');
const Task = require('../models/Task');
const auth = require('../middleware/auth');

const router = express.Router();

router.post('/', auth, (req, res) => {

    const tasks = new Task({
        user: req.user._id,
        title: req.body.title,
        description: req.body.description,
        status: req.body.status
    });

    tasks.save()
        .then(result => res.send(result))
        .catch(() => res.sendStatus(400));
});

router.get('/', auth, (req,res) => {
    console.log(req.user);

    Task.find({user: req.user._id})
        .then(tasks => res.send(tasks))
        .catch(() => res.sendStatus(500))
});

router.put('/:id', auth, async (req, res) => {

    const task = await Task.findOne({_id: req.params.id, user: req.user._id });
    if(!task) {
        return res.status(401).send({error: 'Task unknown'})
    }

    task.title = req.body.title;
    task.description = req.body.description;
    task.status = req.body.status;

    task.save()
        .then(result => res.send(result))
        .catch(() => res.sendStatus(400));
});

router.delete('/:id', auth, async (req, res) => {
  const task = await Task.remove({_id: req.params.id, user: req.user._id });

  if(!task) {
    return res.status(401).send({error: 'Task unknown'})
  }

  res.sendStatus(200);

});

module.exports = router;
