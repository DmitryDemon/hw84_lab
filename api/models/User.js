const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const nanoid = require('nanoid');

const SALT_WORK_FACTOR = 10;

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    token: {
        type: String,
        required: true
    }
});

UserSchema.methods.checkPassword = function (password) {
    return bcrypt.compare(password, this.password)
};

UserSchema.methods.generateToken = function () {
    this.token = nanoid()
};

UserSchema.pre('save', async function (next) {
    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
    const hash = await bcrypt.hash(this.password, salt);
    this.password = hash;
    next()
});

UserSchema.set('toJSON', {
    transform: (doc,ref,options) =>{
        delete  ref.password;
        return ref;
    }
});


const User = mongoose.model('User', UserSchema);

module.exports = User;