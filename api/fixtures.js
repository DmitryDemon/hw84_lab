const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Task = require('./models/Task');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const user = await User.create(
        {
            username: 'Demon',
            password: 1234,
            token: 1,
        },
      {
            username: 'Alex',
            password: 123,
            token: 2,
        }
    );

    await Task.create(
        {
          user: user[0]._id,
          title: 'Do it',
          description: 'Just do it',
          status: 'new'
        },
        {
          user: user[0]._id,
          title: 'Купи молока',
          description: 'Обезжиренное',
          status: 'in_progress'
        },
        {
          user: user[0]._id,
          title: 'Купи жижку',
          description: 'Pink',
          status: 'complete'
        },
        {
          user: user[1]._id,
          title: 'Купить угли',
          description: 'Для кальяна',
          status: 'new'
        },
        {
          user: user[1]._id,
          title: 'Купи табак',
          description: 'Dark side',
          status: 'in_progress'
        },
        {
          user: user[1]._id,
          title: 'Помой кальян',
          description: 'Чательно',
          status: 'complete'
        }
    );

    await connection.close();
};

run().catch(error => {
    console.log('Something went wrong', error);
});